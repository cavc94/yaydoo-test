import { updatePrice } from "../src/services/UpdatePrice.js";
import { CarInsurance } from "../src/models/CarInsurance.js";
import { Product } from "../src/models/Products.js";
import { expect } from "chai";

//La libreria de nyc que funciona con mocha no termina de funcionar, 
//me arroja un coverage pero nada cercano a la realidad.
describe("Co Test", function () {

  it("FullCoverage", function () {
    const data = new CarInsurance([new Product('Full Coverage', 2, 0)]);
    var resultUpdate = updatePrice(data.products);
    const expectResult = new CarInsurance([new Product('Full Coverage', 1, 1)]);
    expect(resultUpdate).to.eql(expectResult.products);
  });

  it("Mega Coverage", function () {
    const data = new CarInsurance([new Product('Mega Coverage', 0, 80)],
      [new Product('Mega Coverage', -1, 80)]);
    var resultUpdate = updatePrice(data.products);
    const expectResult = new CarInsurance([new Product('Mega Coverage', 0, 80)],
      [new Product('Mega Coverage', -1, 80)]);
    expect(resultUpdate).to.eql(expectResult.products);
  });

  it("Special Full Coverage", function () {
    const data = new CarInsurance([
      new Product('Special Full Coverage', 15, 20),
      new Product('Special Full Coverage', 10, 49),
      new Product('Special Full Coverage', 5, 49)]);
    var resultUpdate = updatePrice(data.products);
    const expectResult = new CarInsurance([
      new Product('Special Full Coverage', 14, 21),
      new Product('Special Full Coverage', 9, 50),
      new Product('Special Full Coverage', 4, 50)]);
    expect(resultUpdate).to.eql(expectResult.products);
  });

  it("Super Sale", function () {
    const data = new CarInsurance([
      new Product('Super Sale', 3, 6),
      new Product('Super Sale', -1, 5)]);
    var resultUpdate = updatePrice(data.products);
    const expectResult = new CarInsurance([
      new Product('Super Sale', 2, 4),
      new Product('Super Sale', -2, 3)]);
    expect(resultUpdate).to.eql(expectResult.products);
  });

  it("Generic", function () {
    const data = new CarInsurance([
      new Product('Low Coverage', 5, 7), 
      new Product('Medium Coverage', 10, 20)]);
    var resultUpdate = updatePrice(data.products);
    const expectResult = new CarInsurance([
      new Product('Low Coverage', 4, 6),
      new Product('Medium Coverage', 9, 19)]);
    expect(resultUpdate).to.eql(expectResult.products);
  });

});
