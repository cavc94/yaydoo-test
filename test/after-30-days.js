import { updatePrice } from "../src/services/UpdatePrice.js";
import { CarInsurance } from "../src/models/CarInsurance.js";
import { Product } from "../src/models/Products.js";
import { expect } from "chai";

describe("Co Test", function () {

    it("after-30-days", function () {
        const productsAtDayZero = [
            new Product('Medium Coverage', 10, 20),
            new Product('Full Coverage', 2, 0),
            new Product('Low Coverage', 5, 7),
            new Product('Mega Coverage', 0, 80),
            new Product('Mega Coverage', -1, 80),
            new Product('Special Full Coverage', 15, 20),
            new Product('Special Full Coverage', 10, 49),
            new Product('Special Full Coverage', 5, 49),
            new Product('Super Sale', 3, 6),
        ];

        const carInsurance = new CarInsurance(productsAtDayZero);
        const productPrinter = function (products) {
            console.log(`${products.name}, ${products.sellIn}, ${products.price}`);
        };

        for (let i = 1; i <= 30; i += 1) {
            console.log(`Day ${i}`);
            console.log('name, sellIn, price');
            var result = updatePrice(carInsurance.products);
            result.forEach(element => {
                productPrinter(element)
            });
            console.log('');
        }
    });

});