import { validDecrease, validationRules } from "../Methods.js";

/**
* Funcion para el producto superSale.
* @author   Carlos
* @param    {Object} products    Valor inicial del producto.
*/
function superSale(products) {
    if (validationRules(products)) {
        products.price = validDecrease(products.price, 2);
    }
}

export { superSale };