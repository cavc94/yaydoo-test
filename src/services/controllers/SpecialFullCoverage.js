import { validIncrease, validationRules } from "../Methods.js";

/**
* Funcion para el producto specialFullCoverage.
* @author   Carlos
* @param    {Object} products    Valor inicial del producto.
*/
function specialFullCoverage(products) {
    if (validationRules(products)) {
        products.price = validIncrease(products.price);
    }
    if (products.sellIn < 11) {
        if (validationRules(products)) {
            products.price = validIncrease(products.price);
        }
    }
    if (products.sellIn < 6) {
        if (validationRules(products)) {
            products.price = validIncrease(products.price);
        }
    }
    if (products.sellIn <= 0) {
        products.price = 0;
    }
}

export { specialFullCoverage };