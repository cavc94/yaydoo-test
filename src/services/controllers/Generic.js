import { validDecrease, validationRules } from "../Methods.js";

/**
* Funcion para el producto generic.
* @author   Carlos
* @param    {Object} products    Valor inicial del producto.
*/
function generic(products) {
    if (validationRules(products)) {
        if (products.sellIn > 0) {
            products.price = validDecrease(products.price, 1);
        } else {
            products.price = validDecrease(products.price, 2);
        }
    }
}

export { generic };