import { validIncrease, validationRules } from "../Methods.js";

/**
* Funcion para el producto fullCoverage.
* @author   Carlos
* @param    {Object} products    Valor inicial del producto.
*/
function fullCoverage(products) {
    if (validationRules(products)) {
        products.price = validIncrease(products.price);
        if (products.sellIn <= 0) {
            products.price = validIncrease(products.price);
        }
    }
}

export { fullCoverage };