/**
* Funcion que valida reglas repetitivas
* @author   Carlos
* @param    {Object} products    Objeto de productos.
* @return   {Boolean}            Resultado de las reglas.
*/
function validationRules(products) {
    let result = products.price >= 0 && products.price <= 50 ? true : false;
    return result;
}

/**
* Funcion que valida regla para restar el sellin.
* @author   Carlos
* @param    {Int} value    Valor inicial del producto.
* @param    {Int} decrease    Valor el cual disminuir.
* @return   {Boolean}         Resultado de la resta.
*/
function validDecrease(value, decrease) {
    value = value - decrease;
    value = value < 0 ? 0 : value;
    return value;
}

/**
* Funcion que valida regla para sumar el precio.
* @author   Carlos
* @param    {Int} value    Valor inicial del producto.
* @return   {Boolean}         Resultado de la suma.
*/
function validIncrease(value) {
    value = value + 1;
    value = value > 50 ? 50 : value;
    return value;
}

export { validDecrease, validationRules, validIncrease };