import { fullCoverage } from "./controllers/FullCoverage.js";
import { generic } from "./controllers/Generic.js";
import { megaCoverage } from "./controllers/MegaCoverage.js";
import { specialFullCoverage } from "./controllers/SpecialFullCoverage.js";
import { superSale } from "./controllers/SuperSale.js";

/**
* Funcion para actualizar el precio de los productos.
* @author   Carlos
* @param    {Object} products    Valor inicial del producto.
* @return   {Object}      Resultado de la actualizacion.
*/
function updatePrice(products) {
  for (var i = 0; i < products.length; i++) {
    switch (products[i].name) {
      case 'Full Coverage':
        fullCoverage(products[i]);
        products[i].sellIn = products[i].sellIn - 1;
        break;

      case 'Special Full Coverage':
        specialFullCoverage(products[i]);
        products[i].sellIn = products[i].sellIn - 1;
        break;

      case 'Mega Coverage':
        megaCoverage(products[i]);
        break;

      case 'Super Sale':
        superSale(products[i]);
        products[i].sellIn = products[i].sellIn - 1;
        break;

      default:
        generic(products[i]);
        products[i].sellIn = products[i].sellIn - 1;
        break;
    }
  }
  return products;
}

export { updatePrice };
